#include <ESP8266WiFi.h>
#include <FirebaseESP8266.h>
#include <time.h>
#include <ArduinoOTA.h>
#include <BlynkSimpleEsp8266.h>
#include <SimpleTimer.h>
#include <ModbusMaster.h>
#include <LiquidCrystal_I2C.h>
#include <SoftwareSerial.h>  //  ( NODEMCU ESP8266 )
#include "DHT.h" // esp8266 nodemcu
#define D0 16
#define Lamp1 D0 //กำหนดขาที่นำไปต่อกับรีเลย์
int led_wifi = D3;
LiquidCrystal_I2C lcd(0x3F, 16, 2);
SoftwareSerial pzem(D5, D6); // (RX,TX) connect to TX,RX of PZEM for NodeMCU
//SoftwareSerial pzem(D7,D8);  // (RX,TX) connect to TX,RX of PZEM
#include <ModbusMaster.h>
ModbusMaster node;

BlynkTimer timer;

double amount, temperature, voltage_usage, current_usage, active_power, active_energy, frequency, power_factor, over_power_alarm , over_power , show_all;
#ifndef min
#define min(a,b) ((a)<(b)?(a):(b))
#endif
uint8_t result;
uint16_t data[6];

// ต่อ WiFi
const char* WIFI_SSID     = "OnePlus 7 Pro";
const char* WIFI_PASSWORD = "11111111";

//#define WIFI_SSID     ""
//#define WIFI_PASSWORD ""




// Config Firebase
#define FIREBASE_HOST "ionic3-58c5e.firebaseio.com"
#define FIREBASE_KEY "enXyGFsS3ZgEglFDfnicvlEkCYvYvzZLqyQ3nqMv"
#define DHTPIN  D7    // what digital pin we're connected to
#define DHTTYPE DHT22   // DHT 22
FirebaseData firebaseData;
DHT dht(DHTPIN, DHTTYPE);
float calBill(float Unit, float ft, bool over_150_Unit_per_month = false) ;
int count = 0;

// Config bill
#define FIX_FT -15.90

int timezone = 7;

char ntp_server1[20] = "ntp.ku.ac.th";
char ntp_server2[20] = "fw.eng.ku.ac.th";
char ntp_server3[20] = "time.uni.net.th";

int dst = 0;
float Volt, Amp, Power;
float Energy = 0;


void setup() {
  //  WiFi.mode(WIFI_STA);
  pinMode(led_wifi, OUTPUT);
  digitalWrite(led_wifi, 1);
  // connect to wifi.
  Serial.begin(115200);
  Serial.println("Starting connect wifi...>>>");
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("connecting");

  while (WiFi.status() != WL_CONNECTED) {
    Serial.println("can not connect wifi");
    delay(500);
    digitalWrite(led_wifi, 0);
  }
  Serial.println();
  Serial.print("connected: ");
  Serial.println(WiFi.localIP());
  digitalWrite(led_wifi, 1);

  //--- เซ็ต Relay

  pinMode(Lamp1, OUTPUT); //กำหนดโหมดให้เป็น Output




  Serial.begin(115200);
  dht.begin();
  //  Serial.begin(9600);
  Serial.println("Start serial");
  pzem.begin(9600);
  Serial.println("Start PZEM serial");
  node.begin(1, pzem);
  Serial.println("Start PZEM"); // 1 = ID MODBUS




  configTime(timezone * 3600, dst, ntp_server1, ntp_server2, ntp_server3);
  Serial.println("Waiting for time");
  while (!time(nullptr)) {
    Serial.print(".");
    delay(500);
  }
  Serial.println();
  Serial.println("Now: " + NowString());

  Firebase.begin(FIREBASE_HOST, FIREBASE_KEY);

  lcd.begin();
  lcd.backlight(); // เปิดไฟ backlight


}


void pzemdata() {

  result = node.readInputRegisters(0x0000, 10);

  if (result == node.ku8MBSuccess)
  {
    voltage_usage      = (node.getResponseBuffer(0x00) / 10.0f);
    current_usage      = (node.getResponseBuffer(0x01) / 1000.000f);
    active_power       = (node.getResponseBuffer(0x03) / 10.0f);
    active_energy      = (node.getResponseBuffer(0x05) / 1000.0f);
    frequency          = (node.getResponseBuffer(0x07) / 10.0f);
    power_factor       = (node.getResponseBuffer(0x08) / 100.0f);
    over_power_alarm   = (node.getResponseBuffer(0x09));

    Power = voltage_usage * current_usage;
    Energy += Power;
    
    //    show_all           = (active_power *  NowString());
  }
  Serial.print("ip wifi is : ");
  Serial.println(WiFi.localIP());

  Serial.print("VOLTAGE:           ");   Serial.println(voltage_usage);   // V
  Serial.print("CURRENT_USAGE:     ");   Serial.println(current_usage, 3);  //  A
  Serial.print("ACTIVE_POWER:      ");   Serial.println(active_power);   //  W
  Serial.print("ACTIVE_ENERGY:     ");   Serial.println(active_energy, 3);  // kWh
  Serial.print("FREQUENCY:         ");   Serial.println(frequency);    // Hz
  Serial.print("POWER_FACTOR:      ");   Serial.println(power_factor);
  Serial.print("OVER_POWER_ALARM:  ");   Serial.println(over_power_alarm, 0);
  Serial.print("Power:           ");   Serial.println(Power);
  Serial.print("Energy:           ");   Serial.println(Energy);

  lcd.setCursor(1, 0); // ไปที่ตัวอักษรที่ 0 บรรทัดที่ 0
  lcd.print(voltage_usage);
  lcd.print("V");
  lcd.setCursor(14, 0); // ไปที่ตัวอักษรที่ 0 บรรทัดที่ 1
  lcd.print(current_usage);
  lcd.print("A");
  lcd.setCursor(1, 1); // ไปที่ตัวอักษรที่ 0 บรรทัดที่ 2
  lcd.print(active_power);
  lcd.print("W");
  lcd.setCursor(14, 1); // ไปที่ตัวอักษรที่ 0 บรรทัดที่ 3
  lcd.print (frequency);
  lcd.print("H");
  lcd.setCursor(1, 2); // ไปที่ตัวอักษรที่ 0 บรรทัดที่ 3

  Serial.println("====================================================");
  delay (1000);
}

void loop() {

  ArduinoOTA.handle();
  timer.run();
  pzemdata();
  setdata();
  relayTrick();
  checkAmountconfig();
  senddata();

  // Wait a few seconds between measurements.
  delay(2000);

  float h = dht.readHumidity();
  float t = dht.readTemperature();
  float f = dht.readTemperature(true);
  if (isnan(h) || isnan(t) || isnan(f))
  {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }
  float hif = dht.computeHeatIndex(f, h);
  float hic = dht.computeHeatIndex(t, h, false);
  temperature = t;

  Serial.print("Temperature: ");
  Serial.print(temperature);
  Serial.println(" *C ");



}


void setdata() {
  FirebaseJson data;
  String a = NowString();
  String b = NowString();

  if (count == 1) {
    data.set("time_start", a);
  }
  if (count < 3) {
    count++;
  }

  data.set("voltage_usage", voltage_usage);
  data.set("current_usage", current_usage);
  data.set("active_energy", active_energy);
  if (active_power > 0.0) {

    data.set("active_power", active_power);
    data.set("time_end", b);
    //    active_power =0.0;
  }
  data.set("power_factor", power_factor);
  data.set("frequency", frequency);
  data.set("over_power_alarm", power_factor);
  //  delay(10000);

  if (Firebase.updateNode(firebaseData, "/data_user/1529900123456", data)) {
    Serial.println("update");
  } else {
    Serial.println("Error : " + firebaseData.errorReason());
  }
}


// ----------------------------------------------------
void senddata() {
  FirebaseJson data;
  //  data.set("energy", Energy);
  data.set("voltage_usage", voltage_usage);
  data.set("current_usage", current_usage);
  data.set("active_power", active_power);
  data.set("active_energy", active_energy);
  data.set("frequency", frequency);
  data.set("power_factor", power_factor);
  data.set("over_power_alarm", over_power_alarm);
  data.set("time", NowString());
  if (Firebase.pushJSON(firebaseData, "/Data1529900123456", data)) {
    Serial.println("Added");
  } else {
    Serial.println("Error : " + firebaseData.errorReason());
  }

  amount = calBill(Energy / 1000, FIX_FT, false);
  FirebaseJson temperatureData;
  temperatureData.set("temperature", temperature);
  temperatureData.set("amount", amount);
  //  temperatureData.set("energy", Energy/1000);
  if (Firebase.updateNode(firebaseData, "/config/1529900123456", temperatureData)) {
    Serial.println(amount);
    digitalWrite(led_wifi, 1);
  } else {
    Serial.println("Error : " + firebaseData.errorReason());
    if(firebaseData.errorReason()=="connection lost"){
      digitalWrite(led_wifi, 0);
    }
    
  }
  delay(10000);
}
//-----------------------------------------------------

void checkAmountconfig() {
  if (Firebase.getInt(firebaseData, "/config/1529900123456/maxmoney")) {
    int status = firebaseData.intData();
    Serial.print("Maxamount = ");
    Serial.println(status);
    Serial.print("Amount = ");
    Serial.println(amount);
    if (status == 0) //ถ้าข้อมูลที่เข้ามาคือ 1 , 3 ให้ทำงานตามที่กำหนด
    {

    }
    else if (status <= amount)
    {
      Serial.print("Serial.read()= ");
      Serial.println(Serial.read());
      digitalWrite(Lamp1, 0);
      Serial.print("AmountOver = ");
      Serial.println(status);
      FirebaseJson relayData;
      relayData.set("relay", 0);
      if (Firebase.updateNode(firebaseData, "/config/1529900123456", relayData)) {
        Serial.println(status);
      } else {
        Serial.println("Error : " + firebaseData.errorReason());
      }
    }
  } else {
    Serial.println("Error : " + firebaseData.errorReason());
  }

}

void relayTrick() {
  if (Firebase.getInt(firebaseData, "/config/1529900123456/relay")) {
    int status = firebaseData.intData();

    if (status == 0) //ถ้าข้อมูลที่เข้ามาคือ 1 , 3 ให้ทำงานตามที่กำหนด
    {
      Serial.print("Serial.read()= ");
      Serial.println(Serial.read());
      digitalWrite(Lamp1, 0);
      Serial.print("key: 0 Val= ");
      Serial.println(status);
    }
    else if (status == 1)
    {
      Serial.print("Serial.read()= ");
      Serial.println(Serial.read());
      digitalWrite(Lamp1, 1);
      Serial.print("key: 1 Val= ");
      Serial.println(status);
    }
  } else {
    Serial.println("Error : " + firebaseData.errorReason());
  }
}

float calBill(float Unit, float ft, bool over_150_Unit_per_month) {
  float Service = over_150_Unit_per_month ? 38.22 : 8.19;

  float total = 0;

  if (!over_150_Unit_per_month) {
    float Rate15 = 2.3488;
    float Rate25 = 2.9882;
    float Rate35 = 3.2405;
    float Rate100 = 3.6237;
    float Rate150 = 3.7171;
    float Rate400 = 4.2218;
    float RateMore400 = 4.4217;

    if (Unit >= 6) total += min(Unit, 15) * Rate15;
    if (Unit >= 16) total += min(Unit - 15, 10) * Rate25;
    if (Unit >= 26) total += min(Unit - 25, 10) * Rate35;
    if (Unit >= 36) total += min(Unit - 35, 65) * Rate100;
    if (Unit >= 101) total += min(Unit - 100, 50) * Rate150;
    if (Unit >= 151) total += min(Unit - 150, 250) * Rate400;
    if (Unit >= 401) total += (Unit - 400) * RateMore400;
  } else {
    float Rate150 = 3.2484;
    float Rate400 = 4.2218;
    float RateMore400 = 4.4217;

    total += min(Unit, 150) * Rate150;
    if (Unit >= 151) total += min(Unit - 150, 150) * Rate400;
    if (Unit >= 401) total += (Unit - 400) * RateMore400;
  }

  total += Service;
  total += Unit * (ft / 100);
  total += total * 7 / 100;

  return total;
}

String NowString() {
  time_t now = time(nullptr);
  struct tm* newtime = localtime(&now);

  String tmpNow = "";
  tmpNow += String(newtime->tm_year + 1900);
  tmpNow += "-";
  tmpNow += String(newtime->tm_mon + 1);
  tmpNow += "-";
  tmpNow += String(newtime->tm_mday);
  tmpNow += " ";
  tmpNow += String(newtime->tm_hour);
  tmpNow += ":";
  tmpNow += String(newtime->tm_min);
  tmpNow += ":";
  tmpNow += String(newtime->tm_sec);
  return tmpNow;
}
