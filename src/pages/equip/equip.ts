import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UseequipPage } from '../useequip/useequip';
import { TemperaturePage } from '../temperature/temperature';
import { AngularFireDatabase,AngularFireList } from 'angularfire2/database';
import { Storage } from "@ionic/storage";

/**
 * Generated class for the EquipPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-equip',
  templateUrl: 'equip.html',
})
export class EquipPage {
  username: any;
  maxmoney:any;
  wikiList: AngularFireList<any>;
  wikis = {};
  constructor(public navCtrl: NavController, public navParams: NavParams,public db: AngularFireDatabase,public storage: Storage) {
     this.storage.get("username").then((val) => {
      var string = val.substring(0, 13);
      this.username = string;

      // this.data();
    });
  }

  async ngOnInit() {

    await this.storage.get("username").then((val) => {
      var string = val.substring(0, 13);
      this.username = string;

      // this.data();
    });
    this.wikiList = await this.db.list("config/" + this.username);
    await this.wikiList
      .snapshotChanges()
      .map((actions) => {
        return actions.map((action) => ({
          key: action.key,
          value: action.payload.val(),
        }));
      })
      .subscribe((items) => {
        items.forEach((element) => {
          this.wikis[element.key] = element.value;
        });
      });
    await console.log(this.wikis);
    
    // await this.setdata();
  }

  setdata(){
    // this.maxmoney = this.wikis['maxmoney'];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EquipPage');
  }

  gotoaddequipment(){
    this.navCtrl.push(UseequipPage)
  }

  onGoTo(){

    this.navCtrl.push(TemperaturePage)

  }

  setConfig(data){
    this.db.object(`/config/${this.username}`).update({
      relay: data
    });
  }

  edit(){
    console.log(typeof(this.maxmoney));
  
    this.db.object(`/config/${this.username}`).update({
      maxmoney: parseInt(this.maxmoney)
    });
  }

}
