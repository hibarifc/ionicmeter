import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EquipPage } from './equip';

@NgModule({
  declarations: [
    EquipPage,
  ],
  imports: [
    IonicPageModule.forChild(EquipPage),
  ],
})
export class EquipPageModule {}
