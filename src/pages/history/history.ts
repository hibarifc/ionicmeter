import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';

/**
 * Generated class for the HistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {
  time_using_month_end
  time_using_year_end

  

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    
  }


  
  ionViewDidLoad() {
    console.log('ionViewDidLoad HistoryPage');
  }
  get_time_using_end($event){


    console.log('-------------------------------------------------------')
    // this.time_end = $event;

    // this.time_using_day_end =  $event.day;
    this.time_using_month_end = $event.month;
    this.time_using_year_end = ($event.year)-543;
    // this.time_using_hour_end = $event.hour;
    // this.time_using_minute_end = $event.minute;

    // console.log(this.time_end);
    // console.log(this.time_using_day_end);
    console.log(this.time_using_month_end);
    console.log(this.time_using_year_end);
    // console.log(this.time_using_hour_end);
    // console.log(this.time_using_minute_end);
  }
}

