import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UseequipPage } from './useequip';

@NgModule({
  declarations: [
    UseequipPage,
  ],
  imports: [
    IonicPageModule.forChild(UseequipPage),
  ],
})
export class UseequipPageModule {}
