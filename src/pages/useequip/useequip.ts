import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { AngularFireDatabase,AngularFireList  } from "angularfire2/database";
import { Storage } from "@ionic/storage";
import 'rxjs/add/operator/map'
// import { Injectable } from '@angular/core';
// @Injectable()
/**
 * Generated class for the UseequipPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-useequip",
  templateUrl: "useequip.html"
})
export class UseequipPage {
  uid;
  s;
  username;

  active_energy;
  active_power;
  current_usage;
  frequency;
  over_power_alarm;
  power_factor;
  time_end;
  time_start;
  voltage_usage;
  w;
  money;
  wikiList: AngularFireList<any>;
  wikis={};


  constructor(
    public storage: Storage,
    public db: AngularFireDatabase,
    public navCtrl: NavController,
    public navParams: NavParams
  ) {

   
  
  }

  async ngOnInit() {
    await  this.storage.get("uid").then((val) => {
      this.uid = val;
    });
    await this.storage.get("username").then((val) => {
      var string = val.substring(0, 13);
      this.username = string;
     
      // this.data();
    });
     this.wikiList = await this.db.list("data_user/" + this.username);
     await this.wikiList.snapshotChanges().map(actions => {
      return actions.map(action => ({ key: action.key, value: action.payload.val() }));
      }).subscribe(items => {
        items.forEach(element => {
          this.wikis[element.key] = element.value
          if(element.key=='active_power'){
            this.wikis['watt'] = (element.value/1000).toFixed(3);
            var w2 = element.value/1000
            var p1 = w2 * (-11.6 * 100);
            this.wikis['money']= (((w2 * 2.3488 + p1 + 8.19) * 7) / 100).toFixed(3);
          }
        });
      });
    await console.log( this.wikis);
    
  }

  data() {
    this.money = 0;
    var count = 0;
    console.log(this.username);
    var active_power;
    var time_start;
    var time_end;

    var string_1;
    var string_2;
    var data = "Data" + this.username;

    var count_active_power = 0;
    var count_time = 0;
    var count = 0;
    var data = "Data" + this.username;
    this.s = this.db.database.ref("data_user/" + this.username);
    this.s.once('value', function(snapshot) {
      
      snapshot.forEach(function(childSnapshot) {
        var obj
        var childKey = childSnapshot.key;
        var childData = childSnapshot.val();
        return childData;

      });
      
    });

    this.s.on("child_added", (snap) => {
      if (count == 0) {
        this.active_energy = snap.val();
      } else if (count == 1) {
        this.active_power = snap.val();
        active_power = snap.val();
        console.log(active_power);
      } else if (count == 2) {
        this.current_usage = snap.val();
      } else if (count == 3) {
        this.frequency = snap.val();
      } else if (count == 4) {
        this.over_power_alarm = snap.val();
      } else if (count == 5) {
        this.power_factor = snap.val();
      } else if (count == 6) {
        this.time_end = snap.val();
        var n2 = snap.val().search(" ");
        string_2 = snap.val().substring(n2, snap.val().length);
        var test = string_2.search(":");
        var hour_use = string_2.substring(0, test);
        console.log(+hour_use);
        var cut = string_2.substring(test + 1, string_2.length);
        var test2 = cut.search(":");
        var min_use = cut.substring(0, test2);
        var sec = cut.substring(test2 + 1, cut.length);
        console.log(+min_use);
        console.log(+sec);

        time_end = (+hour_use * 60 + +min_use) * 60 + +sec;
        console.log(time_end);
      } else if (count == 7) {
        this.time_start = snap.val();
        console.log(snap.val());

        var n1 = snap.val().toString().search(" ");
        string_1 = snap.val().toString().substring(n1, snap.val().length);
        var test = string_1.search(":");
        var hour_use = string_1.substring(0, test);
        console.log(+hour_use);
        var cut = string_1.substring(test + 1, string_1.length);
        var test2 = cut.search(":");
        var min_use = cut.substring(0, test2);
        var sec = cut.substring(test2 + 1, cut.length);
        console.log(+min_use);
        console.log(+sec);

        time_start = (+hour_use * 60 + +min_use) * 60 + +sec;
        console.log(time_start);
      } else if (count == 8) {
        console.log((time_end - time_start) / 60 / 60);
        var w2 = (active_power / 1000) * ((time_end - time_start) / 60 / 60);
        console.log(w2);
        this.w = w2.toFixed(3);
        console.log(this.w);
        var p1 = w2 * (-11.6 * 100);
        this.money = (((w2 * 2.3488 + p1 + 8.19) * 7) / 100).toFixed(3);
        this.voltage_usage = snap.val();
        console.log;
      }
      count++;
    });
  }
}
