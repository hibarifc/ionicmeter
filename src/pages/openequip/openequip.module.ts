import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OpenequipPage } from './openequip';

@NgModule({
  declarations: [
    OpenequipPage,
  ],
  imports: [
    IonicPageModule.forChild(OpenequipPage),
  ],
})
export class OpenequipPageModule {}
