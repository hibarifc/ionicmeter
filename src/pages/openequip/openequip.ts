import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { EquipPage } from '../equip/equip';
/**
 * Generated class for the OpenequipPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-openequip',
  templateUrl: 'openequip.html',
})
export class OpenequipPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }








  

  
  GoTo(){

    this.navCtrl.push(EquipPage)

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OpenequipPage');
  }

}
