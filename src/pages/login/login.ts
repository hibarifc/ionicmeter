import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , AlertController } from 'ionic-angular';
import {RegisterPage} from '../register/register';
import { Storage } from '@ionic/storage';
import firebase from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, AngularFireDatabaseModule} from 'angularfire2/database';
import { HomePage } from '../home/home'
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  check_goto
  username
  password
  connect_check_login
  constructor(    
    public storage : Storage , 
    public db:AngularFireDatabase,
    private alertCtrl : AlertController ,
    private fire:AngularFireAuth, 
    public navCtrl: NavController, 
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  
  gotoregister(){
    console.log("เข้าแล้ว TESTregister")
    this.navCtrl.push(RegisterPage);
  }

  getstorage_face_google(uid,username,tel,name,lastname,category_user_select,address,photoURL,sex){
    this.storage.set('uid',uid);
    this.storage.set('username',username);
    this.storage.set('tel',tel);
    this.storage.set('name',name);
    this.storage.set('lastname',lastname);
    this.storage.set('photoURL',photoURL);
    this.storage.set('sex',sex);
  }

  getstorage(uid,username,tel,name,lastname,category_user_select,address,photoURL,sex,password){
    console.log(username);
    this.storage.set('uid',uid);
    this.storage.set('username',username);
    this.storage.set('tel',tel);
    this.storage.set('name',name);
    this.storage.set('lastname',lastname);
    this.storage.set('photoURL',photoURL);
    this.storage.set('sex',sex);
    this.storage.set('password',password);
    console.log(password);
    console.log()
  }



  alert(message:string){
    this.alertCtrl.create({
      // title: 'info!',
      subTitle: message,
      buttons:['OK'],

    }).present();

  }
  signInUser(){
    if(this.username != null && this.password != null ){
      var username_low= this.username.toLowerCase()
      this.fire.auth.signInWithEmailAndPassword(username_low+'@domian.xta',this.password)
      .then(data=>{
        console.log('test');
        this.check_cat()
      })
      .then(data =>{
        // this.check_cat()
       console.log('got some data',this.fire.auth.currentUser);
       this.alert('ยินดีต้อนรับเข้าสู่ระบบ')
      })
      .catch(error =>{
        this.alert('กรุณาป้อนรหัสบัตรประชาชนและรหัสผ่านอีกครั้ง')
        console.log('got an error',error);
      })
  
      console.log('Would register user with',username_low,this.password);
    }

  }
  signOutuser(){
    this.fire.auth.signOut();
  }


  
 
  adduser(uid , email , photoURL){
    let data ={
      uid : uid ,
      username : email ,
      photoURL : photoURL,
      // category_user_select : 'null',
    }
    this.db.object('user/'+uid).update(data);
    this.db.object('locationnaja/'+uid).update(data);
  } 



  nofeace_google(){
    this.alert('อยู่ระหว่างการปรับปรุง');
  }

  check_cat(){
    console.log('check_cat')
    this.connect_check_login = this.db.database.ref('user').orderByChild('username').equalTo((this.username+'@domian.xta').toLowerCase());
    this.connect_check_login.on('child_added',snap =>{

      console.log(snap.val().sex);
      console.log(snap.val().password);
      this.getstorage(snap.val().uid,snap.val().username,snap.val().tel,snap.val().name,snap.val().lastname
      ,snap.val().category_user_select,snap.val().address,snap.val().photoURL,snap.val().sex,snap.val().password);
      
      console.log(snap.val().photoURL);
    // this.storage.get('category_user_select').then((val) => {
    //   console.log('Your username is', val);
        this.navCtrl.setRoot(HomePage)
      // if(this.check_goto == 1){
      //   this.navCtrl.setRoot(UsebuyPage);
      //  }else if(this.check_goto == 2){
      //   this.navCtrl.setRoot(UsebuyPage);
      //  }else if(this.check_goto == 3){
      //   this.navCtrl.setRoot(UsesellPage);
      //  }else if(this.check_goto == 99){
      //   this.navCtrl.setRoot(SelectdegreebuyPage);
      //  }
    // });

     
    })
  }

}
