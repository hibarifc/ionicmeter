import { LoginPage } from "../login/login";
import { RegisterPage } from "../register/register";
import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
} from "ionic-angular";
import { AngularFireDatabase, AngularFireList } from "angularfire2/database";
import { Storage } from "@ionic/storage";
import { constructDependencies } from "@angular/core/src/di/reflective_provider";
import { ConnectPage } from "../connect/connect";

@Component({
  selector: "page-home",
  templateUrl: "home.html",
})
export class HomePage {
  token: string = ''
  uid;
  s;
  money;
  w;
  username;
  set_time = 60;
  wikiList: AngularFireList<any>;
  wikis = {};
  listdata: any=[]
  constructor(
    public loadingCtrl: LoadingController,
    public storage: Storage,
    public db: AngularFireDatabase,
    public navCtrl: NavController,
    public navParams: NavParams
  ) {}

  async ngOnInit() {
    await this.storage.get("uid").then((val) => {
      this.uid = val;
    });
    await this.storage.get("username").then((val) => {
      var string = val.substring(0, 13);
      this.username = string;

      // this.data();
    });

    this.wikiList = await this.db.list("data_user/" + this.username);
    await this.wikiList
      .snapshotChanges()
      .map((actions) => {
        return actions.map((action) => ({
          key: action.key,
          value: action.payload.val(),
        }));
      })
      .subscribe((items) => {
        var count_active_power = 0;
        var count_time = 0;
        items.forEach((element) => {
          // console.log(element.value);
          this.listdata.push(element.value)
          
          this.wikis[element.key] = element.value;
          if (element.key == "active_power") {
            this.wikis["watt"] = (element.value / 1000).toFixed(3);
            var w2 = element.value / 1000;
            var p1 = w2 * (-11.6 * 100);
            this.wikis["money"] = (
              ((w2 * 2.3488 + p1 + 8.19) * 7) /
              100
            ).toFixed(3);
          }
        });
      });
    await console.log(this.listdata);
  }

  number_money() {
    var count = 0;
    this.money = 0;
    this.w = 0;
    console.log(this.username);
    var active_power;
    var time_start;
    var time_end;

    var string_1;
    var string_2;
    var data = "Data" + this.username;

    var count_active_power = 0;
    var count_time = 0;
    this.s = this.db.database.ref(data);
    this.s.on("child_added", (snap) => {
      count_active_power++;
      count_time = count_time + this.set_time;
      console.log(count_time / 60);

      active_power = snap.val().active_power / count_active_power;
      console.log(active_power);

      var w2 = (active_power / 1000) * (count_time / 60);
      console.log(w2);
      this.w = w2.toFixed(3);
      console.log(this.w);
      var p1 = w2 * (-11.6 * 100);
      this.money = (((w2 * 2.4346 + p1 + 8.19) * 7) / 100).toFixed(3);

      // if(count ==0){

      // }else if(count ==1){
      //   active_power = snap.val()
      //   console.log(active_power)
      // }else if(count ==2){

      // }else if(count ==3){

      // }else if(count ==4){

      // }else if(count ==5){

      // }else if(count ==6){
      //   var n2 = snap.val().search(" ");
      //   string_2 = snap.val().substring(n2, (snap.val().length));
      //   var test = string_2.search(":");
      //   var hour_use = string_2.substring(0, test);
      //   console.log(+hour_use)
      //   var cut = string_2.substring(test+1, string_2.length);
      //   var test2 = cut.search(":");
      //   var min_use = cut.substring(0,test2)
      //   var sec = cut.substring(test2+1,cut.length)
      //   console.log(+min_use)
      //   console.log(+sec)

      //   time_end=((((+hour_use)*60)+(+min_use))*60)+(+sec)
      //   console.log(time_end)

      // }else if(count ==7){
      //   var n1 = snap.val().search(" ");
      //   string_1 = snap.val().substring(n1, (snap.val().length));
      //   var test = string_1.search(":");
      //   var hour_use = string_1.substring(0, test);
      //   console.log(+hour_use)
      //   var cut = string_1.substring(test+1, string_1.length);
      //   var test2 = cut.search(":");
      //   var min_use = cut.substring(0,test2)
      //   var sec = cut.substring(test2+1,cut.length)
      //   console.log(+min_use)
      //   console.log(+sec)

      //   time_start=((((+hour_use)*60)+(+min_use))*60)+(+sec)
      //   console.log(time_start)
      // }else if(count ==8){
      //   console.log((((((time_end-time_start)/60)/60))))
      //   var w2 =  (active_power/1000)*(((((time_end-time_start)/60)/60)));
      //   console.log(w2)
      //   this.w = w2.toFixed(3)
      //   console.log(this.w)
      //   var p1=w2*15.86;
      //   this.money = (((w2*85.21)+p1)*7)/100;
      // }
      // count++;
    });
  }

  onGoToUsers() {
    this.navCtrl.push(ConnectPage);
  }
}
