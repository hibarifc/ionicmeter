import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ActionSheetController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, AngularFireDatabaseModule} from 'angularfire2/database';
import { LoginPage } from '../login/login';
import { FormControl } from '@angular/forms';
import { Storage } from '@ionic/storage';
import firebase from 'firebase';
@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  username;
  password;
  password_2;
  name;
  lastname;
  tel;
  category_user_select = "sell";
  sex
  s;

  // photoURL= '../../assets/imgs/money.png'
  
  
  captureDataUrl:string ;
  photoURL:string;
  arr_edit_use={
    uid:'',
    name:'',
    lastname:'',
    tel:'',
    sex:'เลือก',
    photoURL :'../../assets/imgs/money.png',
  }
  buttonColor: string='#rgb(103, 113, 255)';

  constructor( public storage: Storage,
    public loadingCtrl: LoadingController, 
    public actionSheetCtrl: ActionSheetController,
    
    public db:AngularFireDatabase ,private alertCtrl:AlertController, private fire: AngularFireAuth, 
    public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }
  goto_login(){
    this.navCtrl.push(LoginPage)
  }

  alert(message:string,top){
    this.alertCtrl.create({
      title: top,
      subTitle: message,
      buttons:['OK'],
      
    }).present();
  }



  adduser(uid , email,name,lastname,tel,category_user_select,photoURL,sex){
    var email_lower = email.toLowerCase()

    let data ={
      uid : uid ,
      username : email_lower ,
      status : {
        value : 'off'
      } ,
      name:name,
      lastname:lastname,
      tel:tel,
      category_user_select:category_user_select,
      photoURL:photoURL,
      sex:sex,
      // category_user_select : 'null',
    }
    // this.db.object('user/'+uid).update(data);
    this.db.object('locationnaja/'+uid).update(data);
  }

  registerUser(){
    
    if(this.username == null || this.password == null || this.password_2 == null || this.name == null
      || this.lastname == null || this.tel == null || this.sex == 'เลือก'){
        this.alert('กรุณาป้อนข้อมูลให้ครบถ้วน ','แจ้งเตือน!!')
    }else{

      if(this.password == this.password_2  ){
        console.log('เท่านะครับ')
  
        
        this.fire.auth.createUserWithEmailAndPassword(this.username +'@domian.xta',this.password)
        .then((data)=>{
  
                this.db.list('user').push({
                uid : data.user.uid,
                username: (this.username+'@domian.xta').toLowerCase(),
                password:this.password,
                name:this.name,
                lastname:this.lastname,
                tel:this.tel,
                sex:this.sex,
              })
              this.adduser(data.user.uid,this.username+'@domian.xta',this.name,
              this.lastname,this.tel,
              this.category_user_select,
              this.arr_edit_use.photoURL,this.sex);
  
        
          // console.log("แล้วละเน้อ") 
          // this.add_realtime_usesell;
          console.log('got data',data);
          this.alert('สามารถล็อนอินเข้าสู่ระบบ','สมัครสมาชิกสำเร็จ')
          this.navCtrl.push(LoginPage)
        }
        )
        .catch(error =>{
          console.log('got an error', error.message);
          if(error.message == "The email address is badly formatted."){
            console.log('eieie');
            this.alert('กรุณาป้อนตัวเลข 13 หลัก','แจ้งเตือน!!')
          }
          if(error.message == "Password should be at least 6 characters"){
            this.alert('กรุณาป้อนให้ครบ 6 ตัวอักษร','แจ้งเตือน!!')
          }
          if(error.message == "The email address is already in use by another account."){
            this.alert('ผู้ใช้นี้มีคนอื่นใช้แล้ว กรุณาเปลี่ยนใหม่','แจ้งเตือน!!')
          }
        });
  
      }else{
        this.alert("รหัสผ่านไม่เหมือนกัน กรุณาป้อนให้ถูกต้อง",'แจ้งเตือน!!')
      }

    }

    


    console.log('Would register user with',this.username,this.password);
  }

  clear(){
    this.username="";
    this.password="";
    this.password_2="";
    this.name="";
    this.lastname="";
    this.tel="";
    this.sex = 'เลือก';
    this.arr_edit_use.photoURL='../../assets/imgs/money.png';
    

  }
  checkbox(){
    console.log('sadasda');
  }
  select_sex($event){
    this.sex=$event;
    console.log(this.sex);
  }




}
