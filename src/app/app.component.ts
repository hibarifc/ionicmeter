import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { EquipPage } from '../pages/equip/equip';
import { HistoryPage } from '../pages/history/history'
import { Storage } from '@ionic/storage';
import {LoginPage}  from '../pages/login/login'
import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any,icon: any}>;

  constructor(public storage:Storage,private fire: AngularFireAuth,public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'หน้าหลัก', component: HomePage,icon: 'ios-home' },
      { title: 'พลังงานไฟฟ้า', component: EquipPage,icon: 'md-speedometer' },
      { title: 'ประวัติการใช้งาน', component: HistoryPage,icon: 'ios-podium' },
    
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  logout(){
    // this.array_buy = [];


    this.nav.setRoot(LoginPage);


    this.fire.auth.signOut();

    this.storage.remove('uid');
    this.storage.remove('username');
    this.storage.remove('tel');
    this.storage.remove('name');
    this.storage.remove('lastname');
    this.storage.remove('category_user_select');
    this.storage.remove('address');
    this.storage.remove('photoURL');
    this.storage.remove('sex');
    this.storage.remove('password');
    // this.array_data.name = null;
    // this.array_data.photoURL= '';
    // this.array_data.category_user_select = '';

    this.storage.get('uid').then((val) => {
      console.log('Your age is', val);
    });
    this.storage.get('username').then((val) => {
      console.log('Your age is', val);
    });
    this.storage.get('tel').then((val) => {
      console.log('Your age is', val);
    });
    this.storage.get('name').then((val) => {
      console.log('Your age is', val);
    });
    this.storage.get('lastname').then((val) => {
      console.log('Your age is', val);
    });
    this.storage.get('category_user_select').then((val) => {
      console.log('Your age is', val);
    });
    this.storage.get('address').then((val) => {
      console.log('Your age is', val);
    });
    this.storage.get('photoURL').then((val) => {
      console.log('Your age is', val);
    });
    this.storage.get('sex').then((val) => {
      console.log('Your age is', val);
    });
    
  }
}
