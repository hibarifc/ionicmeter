import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register'
import { HistoryPage } from '../pages/history/history'
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule} from 'angularfire2/database'; 
import { AngularFireModule } from 'angularfire2';
import { IonicStorageModule } from '@ionic/storage';
import { EquipPage } from '../pages/equip/equip';
import { UseequipPage } from '../pages/useequip/useequip';
import { ConnectPage } from '../pages/connect/connect';
import { TemperaturePage } from '../pages/temperature/temperature';
import { OpenequipPage } from '../pages/openequip/openequip';


// import { from } from 'rxjs/observable/from';
const firebaseAuth ={
  
  apiKey: "AIzaSyBBXrs-4YZX4A18fL6syawwxDVMr-nzVYQ",
  authDomain: "ionic3-58c5e.firebaseapp.com",
  databaseURL: "https://ionic3-58c5e.firebaseio.com",
  projectId: "ionic3-58c5e",
  storageBucket: "ionic3-58c5e.appspot.com",
  messagingSenderId: "675090957482",
  appId: "1:675090957482:web:0a9687e688d97199ce3cd3",
  measurementId: "G-89NNEHJDR0"
  
};
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    HistoryPage,
    EquipPage,
    UseequipPage,
    ConnectPage,
    TemperaturePage,
    OpenequipPage,


  ],
  imports: [
    IonicStorageModule.forRoot(),
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseAuth),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    HistoryPage,
    EquipPage,
    UseequipPage,
    ConnectPage,
    TemperaturePage,
    OpenequipPage,
    

  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
